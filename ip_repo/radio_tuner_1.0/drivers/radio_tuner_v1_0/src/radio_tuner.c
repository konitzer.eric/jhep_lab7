

/***************************** Include Files *******************************/
#include "radio_tuner.h"
#include "xil_types.h"
#include "xil_io.h"

/************************** Function Definitions ***************************/
#define DDS_FCLK		125000000
#define DDS_NBITS		27

/* Convert string argument in Hz to DDS frequency tuning word */
uint32_t hertz_to_ftw32(float freq_hz)
{
	return (uint32_t) (((freq_hz)/(DDS_FCLK))*(1<<DDS_NBITS));
}

//Convert FTW to Frequency in Hz for dumb humans
double ftwToHz(uint32_t ftw)
{
	return ((double)ftw*DDS_FCLK)/(1<<DDS_NBITS);
}

void radioTuner_setAdcFreq(u32 BaseAddress, float freq)
{
	uint32_t Data = hertz_to_ftw32(freq);
	RADIO_TUNER_mWriteReg(BaseAddress, RADIO_TUNER_S00_AXI_SLV_REG0_OFFSET, Data);
	return;
}

void radioTuner_tuneRadio(u32 BaseAddress, float tune_frequency)
{
	uint32_t Data = hertz_to_ftw32(tune_frequency);
	RADIO_TUNER_mWriteReg(BaseAddress, RADIO_TUNER_S00_AXI_SLV_REG1_OFFSET, Data);
	return;
}

// Write 1 to LSB of 3rd register
void radioTuner_controlReset(u32 BaseAddress, u8 resetval)			// 1 = reset
{
	RADIO_TUNER_mWriteReg(BaseAddress, RADIO_TUNER_S00_AXI_SLV_REG2_OFFSET, resetval&0x01);
	return;
}
