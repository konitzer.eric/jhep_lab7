----------------------------------------------------------------------------------
-- radio_ddc_top.vhd
-- Eric Konitzer
-- All HDL connected IP componentry needed for direct digital downconverter
-- implemented with a master AXI-Stream output interface and AXI lite compatible
-- register input interface for controlling tuning words.
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity radio_ddc_top is
  Port (resetn : in STD_LOGIC;                                  -- Active low reset signal 
        sysclk: in STD_LOGIC;                                   -- 125 MHz system clock
        TUNER_PINC : in STD_LOGIC_VECTOR ( 31 downto 0 );       -- Phase increment word of Tuner DDS
        ADC_PINC: in STD_LOGIC_VECTOR ( 31 downto 0 );          -- Phase increment word of Fake ADC DDS
        DDS_reset : in STD_LOGIC;                               -- Active HIGH reset to DDSs
        m_axis_data_tvalid : out STD_LOGIC;                         -- Output AXI-S t-valid signal
        m_axis_data_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 )    -- Output AXI-S Data, 16 bit I and Q concatenated: MSW: I channel, LSW: Q Channel
  );
end radio_ddc_top;

architecture Behavioral of radio_ddc_top is

    -- Signal Generating DDS
    COMPONENT dds_sig_gen
        PORT (
            aclk : IN STD_LOGIC;
            aresetn : IN STD_LOGIC;
            s_axis_phase_tvalid : IN STD_LOGIC;
            s_axis_phase_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            m_axis_data_tvalid : OUT STD_LOGIC;
            m_axis_data_tdata : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
        );
    END COMPONENT;

    -- Digital Downconverter Tuner DDS (sine and cosine out)
    COMPONENT tuner_dds
        PORT (
            aclk : IN STD_LOGIC;
            aresetn : IN STD_LOGIC;
            s_axis_phase_tvalid : IN STD_LOGIC;
            s_axis_phase_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            m_axis_data_tvalid : OUT STD_LOGIC;
            m_axis_data_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
        );
    END COMPONENT;

    -- FIR Filter Stage 1
    COMPONENT fir_stage_1
        PORT (
            aclk : IN STD_LOGIC;
            s_axis_data_tvalid : IN STD_LOGIC;
            s_axis_data_tready : OUT STD_LOGIC;
            s_axis_data_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            m_axis_data_tvalid : OUT STD_LOGIC;
            m_axis_data_tdata : OUT STD_LOGIC_VECTOR(39 DOWNTO 0)
        );
    END COMPONENT;

    -- FIR Filter Stage 2
    COMPONENT fir_stage_2
        PORT (
            aclk : IN STD_LOGIC;
            s_axis_data_tvalid : IN STD_LOGIC;
            s_axis_data_tready : OUT STD_LOGIC;
            s_axis_data_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            m_axis_data_tvalid : OUT STD_LOGIC;
            m_axis_data_tdata : OUT STD_LOGIC_VECTOR(39 DOWNTO 0)
        );
    END COMPONENT;

    signal dds_amp_out : STD_LOGIC_VECTOR(15 DOWNTO 0);

    signal s_axis_phase_tvalid : STD_LOGIC;
    signal s_axis_phase_tdata : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axis_data_tvalid_adc : STD_LOGIC;

    signal fir1_out_i, fir2_out_i   : STD_LOGIC_VECTOR(39 DOWNTO 0);    -- Raw output of Xiling FIR Compiler core
    signal audio_filt16_i         : STD_LOGIC_VECTOR(15 downto 0);    -- Selected 16 bit field for playback
    signal s_axis_data_tready_f1out_i, m_axis_data_tvalid_f1out_i   : STD_LOGIC; -- AXI-S signals for fir1 stage 1 filter core
    signal s_axis_data_tready_f2out_i, m_axis_data_tvalid_f2out_i   : STD_LOGIC; -- AXI-S signals for fir1 stage 2 filter core
    
    signal fir1_out_q, fir2_out_q   : STD_LOGIC_VECTOR(39 DOWNTO 0);    -- Raw output of Xiling FIR Compiler core
    signal audio_filt16_q         : STD_LOGIC_VECTOR(15 downto 0);    -- Selected 16 bit field for playback
    signal s_axis_data_tready_f1out_q, m_axis_data_tvalid_f1out_q   : STD_LOGIC; -- AXI-S signals for fir1 stage 1 filter core
    signal s_axis_data_tready_f2out_q, m_axis_data_tvalid_f2out_q   : STD_LOGIC; -- AXI-S signals for fir1 stage 2 filter core
    
    
    signal tuner_i, tuner_q : std_logic_vector(15 downto 0);     -- split out in-phase and quadrature outputs of tuner DDS
    signal tunerOut         : std_logic_vector(31 downto 0);
    signal tunerOut_v       : std_logic;
    
    signal mixer_prod_i, mixer_prod_q     : std_logic_vector(31 downto 0);

begin


    s_axis_phase_tvalid <= '1';
    dds_gen : dds_sig_gen
        PORT MAP (
            aclk => sysclk,
            --aresetn => resetn,
            aresetn => not DDS_reset,
            s_axis_phase_tvalid => s_axis_phase_tvalid,
            s_axis_phase_tdata => ADC_PINC,
            m_axis_data_tvalid => m_axis_data_tvalid_adc,
            m_axis_data_tdata => dds_amp_out
        );

    tuner_cplx_inst : tuner_dds
        PORT MAP (
            aclk => sysclk,
            --aresetn => resetn,
            aresetn => not DDS_reset,
            s_axis_phase_tvalid => s_axis_phase_tvalid,     -- Fixed to '1' above
            s_axis_phase_tdata => TUNER_PINC,           -- From proc GPIO
            m_axis_data_tvalid => tunerOut_v,
            m_axis_data_tdata => tunerOut
        );
        
    tuner_i <= tunerOut(15 downto 0);
    tuner_q <= tunerOut(31 downto 16);
    
    mixer_prod_i <= std_logic_vector(signed(tuner_i)*signed(dds_amp_out));
    mixer_prod_q <= std_logic_vector(signed(tuner_q)*signed(dds_amp_out));
    

    fir1_inst_i: fir_stage_1
        PORT MAP (
            aclk => sysclk,
            s_axis_data_tvalid => m_axis_data_tvalid_adc,   -- tvalid out from DDS
            s_axis_data_tready => s_axis_data_tready_f1out_i,
            s_axis_data_tdata => mixer_prod_i(28 downto 13),
            m_axis_data_tvalid => m_axis_data_tvalid_f1out_i,
            m_axis_data_tdata => fir1_out_i
        );

    fir2_inst_i: fir_stage_2
        PORT MAP (
            aclk => sysclk,
            s_axis_data_tvalid => m_axis_data_tvalid_f1out_i,   -- tvalid out from stage 1
            s_axis_data_tready => s_axis_data_tready_f2out_i,
            s_axis_data_tdata => fir1_out_i( 32 downto 17),
            m_axis_data_tvalid => m_axis_data_tvalid_f2out_i,
            m_axis_data_tdata => fir2_out_i
        );

    fir1_inst_q: fir_stage_1
        PORT MAP (
            aclk => sysclk,
            s_axis_data_tvalid => m_axis_data_tvalid_adc,   -- tvalid out from DDS
            s_axis_data_tready => s_axis_data_tready_f1out_q,
            s_axis_data_tdata => mixer_prod_q(28 downto 13),
            m_axis_data_tvalid => m_axis_data_tvalid_f1out_q,
            m_axis_data_tdata => fir1_out_q
        );

    fir2_inst_q: fir_stage_2
        PORT MAP (
            aclk => sysclk,
            s_axis_data_tvalid => m_axis_data_tvalid_f1out_q,   -- tvalid out from stage 1
            s_axis_data_tready => s_axis_data_tready_f2out_q,
            s_axis_data_tdata => fir1_out_q( 32 downto 17),
            m_axis_data_tvalid => m_axis_data_tvalid_f2out_q,
            m_axis_data_tdata => fir2_out_q
        );
        
    -- 18 fractional bits out of stage 2 filter
    audio_filt16_i <= fir2_out_i( 33 downto 18);
    audio_filt16_q <= fir2_out_q( 33 downto 18);
    
    -- Subystem axi stream out signals
    m_axis_data_tdata <= audio_filt16_i&audio_filt16_q;
    m_axis_data_tvalid <= m_axis_data_tvalid_f2out_q;       -- arbitrarily chosen to use the tvalid from the Q channel fir 2 instead of I, should be same enough
    
end Behavioral;
